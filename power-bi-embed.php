<?php

/**
 * Plugin Name:       Power BI Embed
 * Description:       Embed Power BI reports using shortcodes.
 * Plugin URI:        https://gitlab.com/leo020588/wordpress-power-bi-embed-plugin
 * Version:           0.1.0
 * Requires at least: 5.8
 * Requires PHP:      7.4
 * Author:            Lionel Garcia
 */

namespace PowerBIEmbed;

define('POWER_BI_EMBED_PLUGIN_DIR', __DIR__);
define('POWER_BI_EMBED_PLUGIN_URL', plugin_dir_url(__FILE__));
define('POWER_BI_EMBED_PLUGIN_FILE', plugin_basename(__FILE__));

require __DIR__ . '/src/PowerBIEmbed/Plugin.php';
require __DIR__ . '/src/PowerBIEmbed/Report.php';
require __DIR__ . '/src/PowerBIEmbed/Storage.php';

$power_bi_embed_storage = new Storage();
$power_bi_embed_plugin = new Plugin($power_bi_embed_storage);
