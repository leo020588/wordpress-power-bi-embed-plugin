<div class="wrap">
	<h1><?=esc_html(get_admin_page_title())?></h1>

	<form action="options.php" method="post">
		<p>Reports list config in JSON format.</p>
		<textarea name="<?=$this->storage->key?>" style="width:100%;font:16px monospace" rows="9"><?=$value?></textarea>
		<?php settings_fields($this->id); submit_button(); ?>
	</form>

	<h2>Config example:</h2>
	<pre style="border:1px solid gray;padding:1rem"><?=json_encode([
		"my-report" => [
			"client_id" => "xxx",
			"group_id"  => "xxx",
			"report_id" => "xxx",
			"username"  => "xxx",
			"password"  => "xxx"
		],
		"other-report" => [
			"client_id" => "yyy",
			"group_id"  => "yyy",
			"report_id" => "yyy",
			"username"  => "yyy",
			"password"  => "yyy"
		]
	], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)?></pre>

	<h2>Shortcut usage:</h2>
	<pre style="border:1px solid gray;padding:1rem">[power-bi-embed name="my-report"]</pre>
</div>
