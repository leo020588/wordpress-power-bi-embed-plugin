<?php

namespace PowerBIEmbed;


class Plugin {

	private $id   = 'power-bi-embed';
	private $name = 'Power BI Embed';

	private $storage;


	public function __construct ($storage) {
		$this->storage = $storage;

		add_shortcode($this->id, [$this, 'doShortcode']);

		if (is_admin()) {
			add_action('admin_menu', [$this, 'setupAdminPage']);
		} else {
			wp_enqueue_script($this->id, POWER_BI_EMBED_PLUGIN_URL . 'powerbi.min.js');
		}
	}


	public function doShortcode ($atts = []) {
		extract(shortcode_atts(['name' => ''], $atts));

		$report = new Report($this->storage, $name);
		$report->display();
	}


	public function setupAdminPage () {
		$storage = $this->storage;

		add_filter('plugin_action_links_' . POWER_BI_EMBED_PLUGIN_FILE, function ($links) {
			$links[] = sprintf('<a href="%s">%s</a>', admin_url("options-general.php?page={$this->id}"), 'Settings');
			return $links;
		});

		add_submenu_page(
			'options-general.php',
			$this->name,
			$this->name,
			'administrator',
			$this->id,
			[$this, 'displayAdminPage']
		);

		register_setting($this->id, $storage->key, [
			'type'    => 'string',
			'default' => '',
		]);

		add_settings_field(
			$storage->key,
			'Embeds (JSON)',
			function () {},
			$this->id
		);

		add_action("pre_update_option_{$storage->key}", function ($value) use ($storage) {
			$json = @json_decode($value, true);

			if (json_last_error() === JSON_ERROR_NONE && is_array($json)) {
				$storage->setConfig($json);
			}

			return $storage->getRaw();
		}, 10, 2);
	}


	public function displayAdminPage () {
		$value = json_encode($this->storage->getConfig(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		require POWER_BI_EMBED_PLUGIN_DIR . '/settings-page.php';
	}

}
