<?php

namespace PowerBIEmbed;


class Report {

	private $storage;
	private $name;

	private $config;
	private $data;


	public function __construct ($storage, $name) {
		$this->storage = $storage;
		$this->name    = $name;

		$this->config = $storage->getConfig($this->name);
		$this->data   = $storage->getReport($this->name);

		$this->validateReport();
	}


	private function save () {
		$this->storage->setReport($this->data, $this->name);
		$this->storage->save();
	}


	private function validateReport () {
		$requires_new_token = is_null($this->data) ||
		                      count($this->data) === 0 ||
		                      time() > intval($this->data['expires_on']);

		if ($requires_new_token) {
			$this->data = $this->loadAccessToken();
			$this->data['embed_url'] = $this->loadEmbedReportUrl();
			$this->save();
		}
	}


	private function loadAccessToken () {
		$client_id = $this->config['client_id'];
		$username  = $this->config['username'];
		$password  = $this->config['password'];

		$url = 'https://login.windows.net/common/oauth2/token';

		$data = $this->requestQuery($url, null, [
			'grant_type' => 'password',
			'scope'      => 'openid',
			'client_id'  => $client_id,
			'username'   => $username,
			'password'   => $password,
			'resource'   => 'https://analysis.windows.net/powerbi/api',
		]);

		foreach ($data as $k => $v) {
			if (!in_array($k, ['access_token', 'expires_on'])) {
				unset($data[$k]);
			}
		}

		return $data;
	}


	private function loadEmbedReportUrl () {
		$group_id     = $this->config['group_id'];
		$report_id    = $this->config['report_id'];
		$access_token = $this->data['access_token'];

		$url = "https://api.powerbi.com/v1.0/myorg/groups/{$group_id}/reports/{$report_id}";

		$data = $this->requestQuery($url, [
			"Authorization: Bearer {$access_token}",
			'Cache-Control: no-cache',
		]);

		return $data && isset($data['embedUrl']) ? $data['embedUrl'] : null;
	}


	private function requestQuery ($url, $headers = null, $post_data = null) {
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if ($headers) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		if ($post_data) {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		} else {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		}

		$response = curl_exec($ch);
		$errno    = curl_errno($ch);
		$error    = curl_error($ch);

		curl_close($ch);

		if ($errno) {
			throw new \Exception("Curl Error ({$errno}): {$error} [{$url}]");
		}

		return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
	}


	public function display () {
		$tag_id = "power-bi-embed-{$this->name}";

		$config = json_encode([
			'type'        => 'report',
			'id'          => $this->config['report_id'],
			'embedUrl'    => $this->data['embed_url'],
			'accessToken' => $this->data['access_token'],
		]);

		printf(
			'<div id="%s" class="power-bi-embed"></div>' .
			'<script>powerbi.embed(document.querySelector("#%s"), %s);</script>',
			$tag_id,
			$tag_id,
			$config
		);
	}
}
