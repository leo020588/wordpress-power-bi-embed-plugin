<?php

namespace PowerBIEmbed;


class Storage {

	public $key = 'power_bi_embed_data';

	private $data = null;


	private function load () {
		if ($this->data) { return; }
		$raw = get_option($this->key, '{"config":{},"reports":{}}');
		$this->data = json_decode($raw, true);
	}


	public function save () {
		$this->load();
		update_option($this->key, $this->getRaw());
	}


	public function getRaw () {
		$this->load();
		$this->clean();
		return json_encode($this->data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}


	private function clean () {
		foreach ($this->data['reports'] as $k => $v) {
			if (!isset($this->data['config'][$k])) {
				unset($this->data['reports'][$k]);
			}
		}
	}


	private function get ($group, $name = null) {
		$this->load();

		if ($name) {
			return isset($this->data[$group][$name]) ? $this->data[$group][$name] : null;
		} else {
			return $this->data[$group];
		}
	}


	private function set ($group, $value, $name = null) {
		$this->load();

		if ($name) {
			$this->data[$group][$name] = $value;
		} else {
			$this->data[$group] = $value;
		}
	}


	public function getConfig ($name = null) {
		return $this->get('config', $name);
	}


	public function setConfig ($value, $name = null) {
		return $this->set('config', $value, $name);
	}


	public function getReport ($name = null) {
		return $this->get('reports', $name);
	}


	public function setReport ($value, $name = null) {
		return $this->set('reports', $value, $name);
	}

}
